import { useDigimons } from "./Providers/digimonsGet";
import "./global.scss"
import { useState } from "react";
import { useFavoriteDigimons } from "./Providers/favoriteDigimons";

interface Digimon {
  name: string;
  level: string;
  img: string;
}

const App = () => {
  const [userInput, setUserInput] = useState<string>("")
  const [filteredDigimons, setFilteredDigimons] = useState<Digimon[]>([])
  const {digimons} = useDigimons();
  const {favorites, addDigimon, deleteDigimon} = useFavoriteDigimons();

  const searchDigimon = (input: string) => {
    setFilteredDigimons(digimons.filter(
      (item) => item.name.toLocaleLowerCase().includes(input.toLocaleLowerCase())
      )) 
    setUserInput("")
  }

  return (
    <>
    <div className="container">
     <div className="input-container">
       <input 
       type="text" 
       value={userInput}
       onChange={e => setUserInput(e.target.value)}
       placeholder="Busque um digimon" 
       className="search-input"
       />
       <button 
       className="search-button" 
       onClick={() => searchDigimon(userInput)}
       >
         Buscar
        </button>
        <button 
       className="search-button" 
       onClick={() => searchDigimon("")}
       >
         Limpar
        </button>
     </div>
     <div className="digimon-container">
       <h1>Digimons</h1>
       <ul className="digimon-list">
         {
           filteredDigimons.length !== 0 ?
           (filteredDigimons.map((item, index) => (
            <li className="digimon-item" key={index}>
              <h2>{item.name}</h2>
              <span>{item.level}</span>
              <img src={item.img} alt={item.name} />
              <button onClick={() => addDigimon(item)}>Adicionar</button>
            </li>
           )))
            :
          (digimons.map((item, index) => (
            <li className="digimon-item" key={index}>
              <h2>{item.name}</h2>
              <span>{item.level}</span>
              <img src={item.img} alt={item.name} />
              <button onClick={() => addDigimon(item)}>Adicionar</button>

            </li>
          )))
         }
       </ul>
     </div>
     <div className="digimon-container">
         <h1>Favoritos</h1>
         <ul className="digimon-list">
          {
            favorites.map((item, index) => (
              <li className="digimon-item" key={index}>
                <h2>{item.name}</h2>
                <span>{item.level}</span>
                <img src={item.img} alt={item.name} />
              <button onClick={() => deleteDigimon(item)}>Remover</button>

              </li>
            ))
          }
         </ul>
       </div>
    </div>
    </>
  );
}

export default App;
