import { createContext, ReactNode, useContext, useState } from "react";

interface Digimon {
  name: string;
  level: string;
  img: string;
}

interface FavoriteDigimonsProviderProps {
  children: ReactNode;
}

interface FavoriteDigimonsProviderData {
  favorites: Digimon[];
  addDigimon: (digimon: Digimon) => void;
  deleteDigimon: (digimon: Digimon) => void;
}

const FavoriteDigimonsContext = createContext<FavoriteDigimonsProviderData>(
  {} as FavoriteDigimonsProviderData
);

export const FavoriteDigimonsProvider = ({
  children,
}: FavoriteDigimonsProviderProps) => {
  const [favorites, setFavorites] = useState<Digimon[]>([]);

  const addDigimon = (digimon: Digimon) => {
    setFavorites([...favorites, digimon]);
  };

  const deleteDigimon = (digimonToBeDeleted: Digimon) => {
    const newList = favorites.filter(
      (digimon) => digimon.name !== digimonToBeDeleted.name
    );
    setFavorites(newList);
  };

  return (
    <FavoriteDigimonsContext.Provider
      value={{ favorites, addDigimon, deleteDigimon }}
    >
      {children}
    </FavoriteDigimonsContext.Provider>
  );
};

export const useFavoriteDigimons = () => useContext(FavoriteDigimonsContext);