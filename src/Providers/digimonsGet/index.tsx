import axios from "axios";
import { createContext, useState, useContext, ReactNode, useEffect } from "react";

interface Digimon {
  name: string;
  level: string;
  img: string;
}

interface DigimonsProviderProps {
  children: ReactNode;
}

interface DigimonsProviderData {
  digimons: Digimon[];
}

const DigimonsContext = createContext<DigimonsProviderData>(
  {} as DigimonsProviderData
);

export const DigimonsProvider = ({children}: DigimonsProviderProps) => {
  const [digimons, setDigimons] = useState<Digimon[]>([])

  useEffect(() => {
    axios.get("https://digimon-api.vercel.app/api/digimon")
    .then(response => setDigimons(response.data))
  }, [])

  return (
    <DigimonsContext.Provider value={{digimons}}>
      {children}
    </DigimonsContext.Provider>
  )
}

export const useDigimons = () => useContext(DigimonsContext)