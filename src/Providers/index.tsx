import { ReactNode } from "react";
import { DigimonsProvider } from "./digimonsGet/index";
import { FavoriteDigimonsProvider } from "./favoriteDigimons";

interface ProvidersProps {
  children: ReactNode;
}

const Providers = ({children}: ProvidersProps) => {
  return (
    <DigimonsProvider>
      <FavoriteDigimonsProvider>

        {children}
      </FavoriteDigimonsProvider>
    </DigimonsProvider>
  )
}

export default Providers
